# Testing of a new feature in ruby 2.4.0 binding.irb
require 'irb'

class Test
  def self.hello(name)
    binding.irb
    puts "Hello #{name}"
  end
end


puts Test.hello('nilton')
