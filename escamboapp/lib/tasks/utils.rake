namespace :utils do
  desc "Cria administradores falsos"
  task generate_admins: :environment do

    puts "Cadastrando Administradores falsos..."
    10.times do
      Admin.create!(  email: Faker::Internet.email,
                      password: "123456",
                      password_confirmation: "123456",
                      name: Faker::Name.name,
                      role: [0,0,1,1,1].sample)
      puts "Administradores falsos Cadastrados...[OK]"
    end
  end

end
